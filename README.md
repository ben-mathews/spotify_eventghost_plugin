# spotify_eventghost_plugin

Eventghost plugin to control Spotify via Spotify's web API

## Description

This project contains an [EventGhost](http://www.eventghost.net/) plugin to
control [Spotify](https://www.spotify.com) via their Web API interface.  It
works by wrapping the Python [spotipy](https://spotipy.readthedocs.io) library
and exposing its functionality to EventGhost.

I've only implemented the basic functionality that I personally require, but it
should be straightforward to expose other functionality of spotipy by using my
existing implementation as a template.  Current functionality:
* Play/Pause
* Next/Previous Track


## Instllation Instructions

### Step 1: Install Python (if necessary) and spotipy
1) EventGhost comes with a stripped-down Python interpreter.  To use this Plug-in, you must install the Python [spotipy](https://spotipy.readthedocs.io) package, which requires a full Python environment.  I recommend installing Python Stackless 2.7.12 (download from [here](http://www.stackless.com/binaries/python-2.7.12150-stackless.msi)), which is what the current version of EventGhost (0.5 RC6, as of April 2020) is based on.  When installing, keep all the default install settings (including installing to C:\Python27)
2) With Python installed, open a command window as Administrator (hit Start, type 'cmd', right click 'Command Prompt' and select 'Run as administrator')
3) In the Administrator command window, navigate to the 'C:\Python27' folder (`cd c:\Python27`) and run `.\Scripts\pip.exe install spotipy` to use pip to download and install spotipy and its dependencies.
4) When pip finishes close the command window.

### Step 2: Install Plug-In
1) Download the Plugin as a Zip file from GitLab

    [<img src="doc/images/Installing_Plugin_Step01.png" width="200" alt="Step 1">](doc/images/Installing_Plugin_Step01.png)

2) Extract the zip file and copy the 'SpotifyWebControl' folder to the 'C:\Program Files (x86)\EventGhost\plugins' folder.  This will require Adminstrator permission.  When complete, you should have a folder, '' with a '__init__.py' file, as shown below.

    [<img src="doc/images/Installing_Plugin_Step02.png" width="200" alt="Step 2">](doc/images/Installing_Plugin_Step02.png)
    
    [<img src="doc/images/Installing_Plugin_Step03.png" width="200" alt="Step 3">](doc/images/Installing_Plugin_Step03.png)

3) Restart EventGhost to get it to detect the new plug-in.

### Step 3: Acquiring a Spotify Developer Client ID

1) Go to Spotify's Developer Dashboard at [https://developer.spotify.com/dashboard/](https://developer.spotify.com/dashboard/).

    [<img src="doc/images/CreateSpotifyClientID_Step01.png" width="200" alt="Step 1">](doc/images/CreateSpotifyClientID_Step01.png)

2) Click on "Create a Client ID".

    [<img src="doc/images/CreateSpotifyClientID_Step02.png" width="200" alt="Step 2">](doc/images/CreateSpotifyClientID_Step02.png)

3) Specify the name, description, and select Mobile and Desktop App.

    [<img src="doc/images/CreateSpotifyClientID_Step03.png" width="200" alt="Step 3">](doc/images/CreateSpotifyClientID_Step03.png)
    
4) Specify that you are developing a non-commercial app.

    [<img src="doc/images/CreateSpotifyClientID_Step04.png" width="200" alt="Step 4">](doc/images/CreateSpotifyClientID_Step04.png)

5) Agree to the Terms of Service.

    [<img src="doc/images/CreateSpotifyClientID_Step05.png" width="200" alt="Step 5">](doc/images/CreateSpotifyClientID_Step05.png)

6) Edit the settings for your new Client ID

    [<img src="doc/images/CreateSpotifyClientID_Step06.png" width="200" alt="Step 6">](doc/images/CreateSpotifyClientID_Step06.png)

7) Add "http://localhost:8889/callback" to the list of Redirect URIs

    [<img src="doc/images/CreateSpotifyClientID_Step07.png" width="200" alt="Step 7">](doc/images/CreateSpotifyClientID_Step07.png)

8) Hit Save button

    [<img src="doc/images/CreateSpotifyClientID_Step08.png" width="200" alt="Step 8">](doc/images/CreateSpotifyClientID_Step08.png)

9) Select "Show Client Secret" to see the Client Secret key

    [<img src="doc/images/CreateSpotifyClientID_Step09.png" width="200" alt="Step 9">](doc/images/CreateSpotifyClientID_Step09.png)

10) The Client ID and Client Secret are visible copy into EventGhost

    [<img src="doc/images/CreateSpotifyClientID_Step10.png" width="200" alt="Step 10">](doc/images/CreateSpotifyClientID_Step10.png)

### Step 4: Configuring EventGhost

1) Hit the "Add Plugin" button in EventGhost.

    [<img src="doc/images/ConfigureEventGhost_Step01.png" width="200" alt="Step 1">](doc/images/ConfigureEventGhost_Step01.png)
 
2) Under "Software Control" select "Spotify Controller"

    [<img src="doc/images/ConfigureEventGhost_Step02.png" width="200" alt="Step 2">](doc/images/ConfigureEventGhost_Step02.png)

3) Enter your Spotify username, and Client ID and Secret (obtained in the previous section)

    [<img src="doc/images/ConfigureEventGhost_Step03.png" width="200" alt="Step 3">](doc/images/ConfigureEventGhost_Step03.png) 

4) Hit the "Update Devices" button to query available devices from Spotify, select the device to control under "Select Device" and hit Ok.  If EventGhost complains about not able to refresh a bad token, try deleting the '.cache-USERNAME' file in %APPDATA\EventGhost (C:\Users\USERNAME\AppData\Roaming\EventGhost).  This frequently happens when changing Client ID's. 

    [<img src="doc/images/ConfigureEventGhost_Step04.png" width="200" alt="Step 4">](doc/images/ConfigureEventGhost_Step04.png)