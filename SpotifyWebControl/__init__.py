# -*- coding: utf-8 -*-
"""EventGhost Spotify Controller

This project contains an EventGhost plugin to control Spotify via their
Web API interface.  It works by wrapping the spotipy library and
exposing functionality to EventGhost.

I've only implemented the basic functionality that I require, but it should be
straightforward to expose other functionality of spotipy.

To use this script you need to register your application
at https://developer.spotify.com/dashboard
  1) Go to https://developer.spotify.com/dashboard
  2) Select "Create A Client ID"
  3) Specify a app name, app description, and what you are building (I selected
     Mobile App and Desktop App)
  4) Select that you are doing non-commercial development
  5) Agree to the TOS
  6) Select the new Client ID that you just created and add
     "http://localhost:8889/callback" as a "Redirect URI"
"""

import os
import time
import platform
import json

import spotipy

import eg
import wx

eg.RegisterPlugin(
    name = "Spotify Controller",
    guid = "{CF1EA81D-7B59-42F7-8055-C62D82DFAD24}",
    author = "Ben Mathews",
    version = "0.0.1",
    kind = "program",
    description = "Provides Basic Control of Spotify via the 'spotipy' Python package",
    icon = (
        "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACNElEQVR42o2TbUhTYRTHfzc3bcuX"
        "EndpiUVvlIRIGjmHFUVFQRALhLIXpCIIghAbfpCiD6OwEX3pQ0ERBAq94IcIAitJwpJwZmVFqdnE"
        "QiWLLNfUuXXu7tQZd+GBh3Pufc7zP+f8n/+jkMCOnEdNtrBci8dH6b5WxaBRnvLvjwuP2ZdkolLC"
        "grj9iCxfeAKvewu3DQGO1jA310GthHv4v93pbOPQlQqCMwAuNlEvzsXs7G7lJkqnAGoeUWYyUxsa"
        "gy9d8K0PAr9kU3at6ZCVDXZhw5w8jRAOs9e9mVtKrHpbJMLas1L/9w/9oCVVfBL8ESCZHZMcXr0e"
        "SmTAlYVRDJ90sU7Zf5qFBVv5qnXT3a5XtOXIAfNUpWhHnT5obYDed3DiMizNI/K+BbtS7sGZt4Fm"
        "LbnnDTTWQX8PjPzUQTJskLMK1pRAbhEM9oK6GOZId10vcWoAxQLwTAPwC/qn15C9AuZlwEQIvvfD"
        "5w549QRSLHDMC5l2vTsBKFbKqrEVbmdAGyE0Dh1PYcAvJA7rVearsCwfFgmJ7Y3Cg0PA03VtvG1G"
        "nSSxRVzR8JCMIEpQl0DqAp28IWHnwwsIjsCBM3r7MXsuJDqjAOce4EqxRnWQ0Po+QlqmcJKlf8uV"
        "767axr14Id0Ud3CWQroh1Q/PUGKpG7NjF1clLDd6I3Fv4rrvIcfrPIQwSvTcZ4cljVMSbpQVUwNC"
        "L03BAN7qnTTE5yeqhOskVrk2jTJlbBR//SUCRnl/AbtOpXRdoA/cAAAAAElFTkSuQmCC"
    )
)


class SpotifyControllerPlugin(eg.PluginBase):
    """
    Spotify Plug-In
    """
    def __init__(self):
        """
        Constructor for SpotifyControllerPlugin
        """
        self.redirect_uri = 'http://localhost:8889/callback/'
        self.scope = 'user-read-playback-state user-modify-playback-state'

        self.sp = None
        self.client_id = None
        self.client_secret = None
        self.username = None
        self.device = None
        self.devices = []

        self.AddAction(UpdateListOfSpotifyDevices)
        self.AddAction(Play)
        self.AddAction(Pause)
        self.AddAction(NextTrack)
        self.AddAction(PreviousTrack)


    def __start__(self,
                  username="SpotifyUserName",
                  client_id="00000000000000000000000000000000",
                  client_secret="00000000000000000000000000000000",
                  device="UNDEFINED",
                  devices=''):
        """
        EventGhost __start__ method
        :param username: Spotify Username
        :param client_id: Spotify Client ID (from https://developer.spotify.com/dashboard)
        :param client_secret: Spotify Client Secret (from https://developer.spotify.com/dashboard)
        :param device: Device name (generally the hostname)
        :param device: Device names, a string with device names separated by semicolons
        :return:
        """
        self.client_id = client_id
        self.client_secret = client_secret
        self.username = username
        self.device = device
        self.devices = devices.split(';')
        self.initialize_spotify()


    def Configure(self,
                  username="SpotifyUserName",
                  client_id="00000000000000000000000000000000",
                  client_secret="00000000000000000000000000000000",
                  device="UNDEFINED",
                  devices=''):
        """
        EventGhost Configure method
        :param username: Spotify Username
        :param client_id: Spotify Client ID (from https://developer.spotify.com/dashboard)
        :param client_secret: Spotify Client Secret (from https://developer.spotify.com/dashboard)
        :param device: Device name (generally the hostname)
        :param device: Device names, a string with device names separated by semicolons
        :return:
        """

        panel = eg.ConfigPanel()
        client_id_ctrl = panel.TextCtrl(client_id, size=(300, -1))
        client_secret_ctrl = panel.TextCtrl(client_secret, size=(300, -1))
        username_ctrl = panel.TextCtrl(username, size=(300, -1))

        def set_device_combobox_items(event):
            """
            Sets the items in the Device combobox
            :param event: wx event
            :return:
            """
            username = username_ctrl.GetValue()
            client_id = client_id_ctrl.GetValue()
            client_secret = client_secret_ctrl.GetValue()
            sp = spotipy.Spotify(
                auth_manager=spotipy.SpotifyOAuth(
                    client_id=client_id,
                    client_secret=client_secret,
                    redirect_uri=self.redirect_uri,
                    scope=self.scope,
                    username=username))
            devices = sp.devices()
            devices = devices['devices']
            device_names = [device['name'] for device in devices]
            device_ctrl.Clear()
            device_ctrl.AppendItems(device_names)
            device_ctrl.SetValue(device_names[0])

        get_devices_btn = panel.Button("Update Devices")
        get_devices_btn.Bind(wx.EVT_BUTTON, set_device_combobox_items)

        if device != "UNDEFINED" and len(devices) != 0:
            device_ctrl = panel.ComboBox(
                value=device,
                choices=devices.split(';'),
                size=(300, -1)
            )
        else:
            device_ctrl = panel.ComboBox(
                value="Empty",
                choices=devices,
                size=(300, -1)
            )

        client_id_label = panel.StaticText("Client ID:")
        client_secret_label = panel.StaticText("Client Secret:")
        username_label = panel.StaticText("Spotify Username:")
        button_label = panel.StaticText("Update Devices:")
        device_label = panel.StaticText("Select Device:")

        eg.EqualizeWidths((client_id_label, username_label,
                           client_secret_label, button_label,
                           device_label))
        settings_box = panel.BoxedGroup(
            "Spotify Settings",
            "1) Get Spotify Client ID and Secret from https://developer.spotify.com/dashboard",
            "    (If necessary, create a Client ID at the dashboard, and edit its settings to add",
            "     'http://localhost:8889/callback' as a Redirect URI)",
            "2) Enter Spotify username, Client ID and Secret into fields below and hit ",
            "   the 'Update Devices' button",
            "3) A web browser may open prompting you to authenticate.  Do so if necessary.",
            "4) The Devices list should be populated.  Select the device you wish to control.",
            "4) Hit 'Ok'",
            (username_label, username_ctrl),
            (client_id_label, client_id_ctrl),
            (client_secret_label, client_secret_ctrl),
            (button_label, get_devices_btn),
            (device_label, device_ctrl),
        )

        panel.sizer.Add(settings_box, 0, wx.EXPAND)

        while panel.Affirmed():
            panel.SetResult(
                username_ctrl.GetValue(),
                client_id_ctrl.GetValue(),
                client_secret_ctrl.GetValue(),
                device_ctrl.GetValue(),
                ';'.join(device_ctrl.GetItems())
            )


    def initialize_spotify(self):
        """
        Initializes self.sp object
        :return: Void
        """
        self.sp = spotipy.Spotify(
            auth_manager=spotipy.SpotifyOAuth(
                client_id=self.client_id,
                client_secret=self.client_secret,
                redirect_uri=self.redirect_uri,
                scope=self.scope,
                username=self.username))
        self.update_devices()


    def find_device(self, device_name):
        """
        Given the Spotify device name, finds the entry in the list of devices
        :param device_name: Spotify device name
        :return:
        """
        devices = [device for device in self.devices
                   if device['name'] == device_name]
        if len(devices) != 1:
            raise Exception(
                "Expecting a single device with name == '" +
                device_name + "'.  Found " + len(devices) + " devices.")
        return devices[0]


    def update_devices(self):
        """
        Updates list of Spotify devices
        :return: Void
        """
        devices = self.sp.devices()
        self.devices = devices['devices']


    def start_playback(self):
        """
        Starts or resumes playback
        :return: Void
        """
        device = self.find_device(self.device)
        self.sp.start_playback(device_id=device['id'])


    def pause_playback(self):
        """
        Pauses playback
        :return: Void
        """
        device = self.find_device(self.device)
        self.sp.pause_playback(device_id=device['id'])


    def next_track(self):
        """
        Next track
        :return: Void
        """
        device = self.find_device(self.device)
        self.sp.next_track(device_id=device['id'])


    def previous_track(self):
        """
        Previous track
        :return: Void
        """
        device = self.find_device(self.device)
        self.sp.previous_track(device_id=device['id'])


class UpdateListOfSpotifyDevices(eg.ActionBase):
    """
    Updates list of Spotify devices
    """
    name = "Update List Of Spotify Devices"
    description = "Updates list of available Spotify devices"
    def __call__(self):
        self.plugin.update_devices()


class Play(eg.ActionBase):
    """
    Starts or resumes playback
    """
    name = "Play"
    description = "Start/resume playback"
    def __call__(self):
        self.plugin.start_playback()


class Pause(eg.ActionBase):
    """
    Pauses playback
    :return: Void
    """
    name = "Pause"
    description = "Pause playback"
    def __call__(self):
        self.plugin.pause_playback()


class NextTrack(eg.ActionBase):
    """
    Next track
    :return: Void
    """
    name = "Next Track"
    description = "Next Track"
    def __call__(self):
        self.plugin.next_track()


class PreviousTrack(eg.ActionBase):
    """
    Previous track
    :return: Void
    """
    name = "Previous Track"
    description = "Previous Track"
    def __call__(self):
        self.plugin.previous_track()